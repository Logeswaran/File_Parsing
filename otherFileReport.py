import re
import xlwt
fo=open("/mnt/etphfs-mnt/download/0211-1542-000/b/otherFiles.txt","r")
fileContent = fo.readlines()
op={}

def constructData():
	
	fileSerial=0

	for line in fileContent:
		stripedLine=line.strip()	

		if stripedLine.startswith('['):
			op[fileSerial]={}
			fileSerial+=1
		elif stripedLine.startswith('\"path\"'):			
			path=(re.sub('\\\/','/',stripedLine[10:])).rstrip('",')
			path=re.sub(r'\\\\ ',r'/',path)
			op[fileSerial-1]['path']=path
		elif  stripedLine.startswith('\"_name\"'):
			op[fileSerial-1]['name']=stripedLine
		elif stripedLine.startswith('\"fileSize\"'):
			fsize=re.search('[0-9]{1,9}',stripedLine)
			op[fileSerial-1]['size']=fsize.group()
		else:
			pass

	print "Total Files===>",fileSerial
	return fileSerial

	
def makeOtherFile():
	totalRecords=constructData()
	nonlocalFile=0
	import xlwt
	from datetime import datetime
	wb = xlwt.Workbook()
	ws = wb.add_sheet('otherFile')
	ws.write(0, 0, "FilePath")
	ws.write(0, 1, "Size")
	row=1
	column=0
	for i in range(0,totalRecords):	
		if ".tintri-sm" in op[i]['name'] or "__defaultEvictionGroup" in op[i]['name']:
			pass
		else:
			if op[i].has_key("path"):
				ws.write(row, column, op[i]['path'])
				column+=1
			else:
				ws.write(row, column, 'None')
				column+=1
			if op[i].has_key("path"):
				ws.write(row, column, op[i]['size'])
			else:
				ws.write(row, column, 'None')
			row+=1
			column-=1

			nonlocalFile+=1

	print "Total Non Local Files===>", nonlocalFile

	#====================================================
	#xlwt save
	#====================================================
	wb.save('example.xls')


makeOtherFile() #set the source file path(.txt) on line 3 

#"path": ".dvsData\/96\\ 4a\\ 25\\ 50\\ c2\\ 04\\ a5\\ c7-ec\\ 2c\\ b2\\ 31\\ 92\\ 39\\ 9b\\ e2\/1042",
#"path": ".tintri-sm\/statsdb\/a\/data-perf\/db9.4\/pg_multixact\/offsets\/0000",
# file=$(echo "$line" | awk -F '"path":' '{print $2}' | sed -e 's/^[ \t]*"//' | sed -e 's/",$//' | sed 's/\\\//\//g' | sed 's/\\\\ /\//g')